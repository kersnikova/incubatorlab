#include <LiquidCrystal.h>

#include <SC16IS750.h>
#include <NDIRZ16.h>
#include <DHT.h>

// DHT
// Which digital pin is DHT sensor connected to
#define DHTPIN 9

// Uncomment only the type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);

// NDIR
SC16IS750 i2cuart = SC16IS750(SC16IS750_PROTOCOL_I2C,SC16IS750_ADDRESS_BB);
NDIRZ16 mySensor = NDIRZ16(&i2cuart);

// Initialize the LCD with the numbers of the interface pins
LiquidCrystal lcd(13, 11, 5, 10, 3, 2);

// Relay variables
const int numRelays = 4;
int relayPins[numRelays] = {4, 7, 8, 12};
int co2relay = 0;
int heatingrelay = 1;

// CO2 variables
long co2open = 0;
int lastco2open = 0;
int co2opencurrent = 0;
long ppm = 0;


/* SETTINGS */

long ppmTreshold = 49000;            // ppm threshold to turn on co2 (10000ppm is 1%)
int co2waitafteropen = 45;           // seconds to wait between each opening of the valve
int co2openseconds = 1;              // how many seconds should the co2 valve be open
float heatingLimits[2] = {36.8, 37.2}; // low threshold (heater on temperature) and high threshold (heater off temperature)

/* SETTINGS */




// Power control function for NDIR sensor. 1=ON, 0=OFF
void power (uint8_t state) {
  i2cuart.pinMode(0, INPUT);    // set up for the power control pin
 
  if (state) {
    i2cuart.pinMode(0, INPUT);  // turn on the power of MH-Z16
  } else {
    i2cuart.pinMode(0, OUTPUT);
    i2cuart.digitalWrite(0, 0); // turn off the power of MH-Z16
  }
}

// Zero Point (400ppm) Calibration function for NDIR sensor. Only use when necessary.
// Make sure that the sensor has been running in fresh air (~400ppm) for at least 20 minutes before calling this function.
void calibrate() {
  Serial.println("NDIR sensor calibration start");
  i2cuart.pinMode(1, OUTPUT);    //set up for the calibration pin.
     
  i2cuart.digitalWrite(1, LOW);  //start calibration of MH-Z16 under 400ppm
  delay(10000);                  //5+ seconds needed for the calibration process
  i2cuart.digitalWrite(1, HIGH); //toggle the pin HIGH back to normal operation
  Serial.println("NDIR sensor calibration done.");
}

void setup() {
  Serial.begin(9600);
  Serial.println("inkubator startup");

  // TODO What is this?
  pinMode(6, OUTPUT);
  analogWrite(6, 40);

  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("Inkubator");
  lcd.setCursor(0,1);
  lcd.print("se priziga...");

  for (int i = 0; i < numRelays; i++) {
    pinMode(relayPins[i], OUTPUT);
    digitalWrite(relayPins[i], HIGH);
  }
  delay(250);

  // NDIR CO2 sensor
  i2cuart.begin(9600);
  digitalWrite(relayPins[0], LOW); 
  delay(250);

  if (i2cuart.ping()) {
    digitalWrite(relayPins[1], LOW);
    Serial.println("SC16IS750 found.");
    Serial.println("Wait 10 seconds for sensor initialization...");
  } else {
    Serial.println("SC16IS750 not found.");
    while(1);
  }
  delay(250);

  dht.begin();
  digitalWrite(relayPins[2], LOW);
  delay(250);

  power(1);
  //Wait for the NDIR sensor to initialize.
  delay(8000);
  // Sensor calibration - run only run if necessary, and read the comments of the calibrate function beforehand!
  bool calibration = false;
  if (calibration) {
    lcd.clear();
    lcd.print("Inkubator");
    lcd.setCursor(0,1);
    lcd.print("Kalibracija CO2...");
    calibrate();
    lcd.clear();
    lcd.print("Inkubator");
    lcd.setCursor(0,1);
    lcd.print("Kalibracija koncana...");
    delay(100000);
  }

  for (int i = 0; i < numRelays; i++) {
    digitalWrite(relayPins[i], LOW);
  }
}

void loop() {
  delay(1000);

  long ppmOver = 0;
  if (mySensor.measure()) {
    Serial.print("CO2 Concentration is ");
    Serial.print(mySensor.ppm);
    ppm = mySensor.ppm;
    Serial.println("ppm");
    if (mySensor.ppm < ppmTreshold && mySensor.ppm > 200) ppmOver = 1;
  } else {
    Serial.println("NDIR CO2 sensor communication error.");
  }

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print(f);
  Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.print(" *C ");
  Serial.print(hif);
  Serial.println(" *F");

    
  if (ppmOver && lastco2open == 0) {
    digitalWrite(relayPins[co2relay], HIGH);
    lastco2open = co2waitafteropen;
    co2opencurrent = co2openseconds;
  } else if (co2opencurrent > 0) {
    co2opencurrent--;
  } else {
    digitalWrite(relayPins[co2relay], LOW);
  }
  if (lastco2open > 0) lastco2open--;
  
  if (t > heatingLimits[1]) {
    digitalWrite(relayPins[heatingrelay], LOW);
  }
  if (t < heatingLimits[0]) {
    digitalWrite(relayPins[heatingrelay], HIGH);
  }

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("CO2:");
  lcd.setCursor(5,0);
  float ppmperc = ((float)ppm)/ 10000.0;
  lcd.print(ppmperc);
  lcd.setCursor(10,0);
  lcd.print(ppm);
    
  lcd.setCursor(0,1);
  lcd.print("Hum:");
  lcd.setCursor(4,1);
  lcd.print(h);

  lcd.setCursor(10,1);
  lcd.print("T:");
  lcd.setCursor(12,1);
  lcd.print(t);
}

